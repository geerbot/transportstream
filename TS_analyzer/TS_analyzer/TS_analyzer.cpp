// TS_analyzer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using namespace std;

#define DEBUG_LOG 1

enum VideoStatus
{
	VidOk,
	VidError
};

class PTS
{
public:
	unsigned char sync;
	unsigned int pts;
	unsigned char marker_bit_1;
	unsigned char marker_bit_2;
	unsigned char marker_bit_3;
};

class DTS
{
public:
	unsigned char sync;
	unsigned int dts;
	unsigned char marker_bit_1;
	unsigned char marker_bit_2;
	unsigned char marker_bit_3;
};

class ESCR
{
public:
	unsigned char reserved;
	unsigned int escr_base;
	unsigned short escr_extension;
	unsigned char marker_bit_1;
	unsigned char marker_bit_2;
	unsigned char marker_bit_3;
	unsigned char marker_bit_4;
};

class ES
{
public:
	unsigned char marker_bit_1;
	unsigned int ES_rate;
	unsigned char marker_bit_2;
};

class DSM
{
public:
	unsigned char trick_mode_control;
	unsigned char field_id;
	unsigned char intra_slice_refresh;
	unsigned char frequency_truncation;
	unsigned char rep_cntrl;
	unsigned char reserved;
};

class ADDITIONAL
{
public:
	unsigned char marker_bit;
	unsigned char copy_info;
};

class CRC
{
public:
	unsigned short previous_CRC;
};

class PesExtension
{
public:
	unsigned char private_data_flag;
	unsigned char pack_header_field_flag;
	unsigned char program_packet_sequence_counter_flag;
	unsigned char P_STD_buffer_flag;
	unsigned char reserved;
	unsigned char extension_flag_2;
	unsigned char * private_data;
	unsigned char pack_Field_length;
	unsigned char marker_bit1;
	unsigned char program_packet_sequence_counter;
	unsigned char marker_bit2;
	unsigned char MPEG1_MPEG2_identifier;
	unsigned char original_stuff_length;
	unsigned char zero_one;
	unsigned char P_STD_buffer_scale;
	int P_STD_buffer_size;
	unsigned char marker_bit3;
	unsigned char PES_extension_field_length;
};

class PES
{
public:
	int packet_start_code_prefix;
	unsigned char stream_id;
	unsigned short PES_packet_length;
	unsigned char one_zero;
	unsigned char PES_scrambling_control;
	unsigned char PES_priority;
	unsigned char data_alignment_indicator;
	unsigned char copyright;
	unsigned char original_or_copy;
	unsigned char PTS_DTS_flags;
	unsigned char ESCR_flag;
	unsigned char ES_rate_flag;
	unsigned char DSM_trick_mode_flag;
	unsigned char additional_copy_info_flag;
	unsigned char PES_CRC_flag;
	unsigned char PES_extension_flag;
	unsigned char PES_header_data_length;

	PTS pts;
	DTS dts;
	ESCR escr;
	ES es;
	DSM dsm;
	ADDITIONAL additional;
	CRC crc;
	PesExtension ext;
};

class PAT
{
public:
	unsigned char table_id;
	unsigned char section_syntax_indicator;
	unsigned char zero;
	unsigned char reserved;
	unsigned char section_length;
	unsigned short transport_stream_id;
	unsigned char reserved2;
	unsigned char version_number;
	unsigned char current_next_indicator;
	unsigned char section_number;
	unsigned char last_section_number;
	vector<unsigned short>program_number;
	vector<unsigned char>reserved3;
	vector<unsigned short>network_PID;
	vector<unsigned short>program_map_PID;
	unsigned int CRC_32;
private:
	enum TableType
	{
		ProgramAssocationTableType = 0,
		ConditionalAccessTableType = 1,
		TsProgramMapSectionType = 2,
		TsDescriptionSectionType = 3,
		Mpeg4SceneDescriptionType = 4,
		Mpeg4ObjectDescriptionType = 5
	};
};

class PMT
{
public:
	unsigned char table_id;
	unsigned char section_syntax_indicator;
	unsigned char zero;
	unsigned char reserved;
	unsigned short section_length;
	unsigned short program_number;
	unsigned char reserved2;
	unsigned char version_number;
	unsigned char current_next_indicator;
	unsigned char section_number;
	unsigned char last_section_number;
	unsigned char reserved3;
	unsigned short PCR_PID;
	unsigned char reserved4;
	unsigned short program_info_length;
	string descriptor;
	vector<unsigned char>stream_type;
	vector<unsigned char>reserved5;
	vector<unsigned short>elementary_PID;
	vector<unsigned char>reserved6;
	vector<unsigned short>ES_info_length;
	vector<string>descriptor2;
	unsigned int CRC_32;
};

class Adaptation
{
public:
	unsigned char length;
	unsigned char discontinuity_indicator;
	unsigned char random_access_indicator;
	unsigned char elementary_stream_priority_indicator;
	unsigned char PCR_flag;
	unsigned char OPCR_flag;
	unsigned char splicing_point_flag;
	unsigned char transport_private_data_flag;
	unsigned char adaptation_field_extension_flag;
	unsigned int program_clock_reference_base;
	unsigned char reserved;
	unsigned short program_clock_reference_extension;
	unsigned int original_program_clock_reference_base;
	unsigned char reserved2;
	unsigned short original_program_clock_reference_extension;
	unsigned char splice_countdown;
	unsigned char transport_private_data_length;
	unsigned char * private_data_byte;
	unsigned char adaptation_field_extension_length;
	unsigned char ltw_flag;
	unsigned char piecewise_rate_flag;
	unsigned char seamless_splice_flag;
	unsigned char reserved3;
	unsigned char ltw_valid_flag;
	unsigned short ltw_offset;
	unsigned char reserved4;
	unsigned int piecewise_rate;
	unsigned char splice_type;
	unsigned char DTS_next_AU;
	unsigned char marker_bit;
	unsigned short DTS_next_AU2;
	unsigned char marker_bit2;
	unsigned short DTS_next_AU3;
	unsigned char marker_bit3;
};

class VideoPacket
{
public:
	unsigned char sync_byte;
	unsigned char transport_error_ind;
	unsigned char unit_start_ind;
	unsigned char priority;
	int program_id;
	unsigned char transport_scramble;
	unsigned char adaptation_field;
	unsigned char continuity_ctr;

	Adaptation adaptation;
	PES pes;
	PAT pat;
	PMT pmt;
};

class VideoHandle
{
public:
	VideoHandle(string file_path);
	VideoStatus VideoOpen();
	VideoStatus NextPacket();
	VideoStatus PrevPacket();
	vector<int> GetPIDs();
	long GetTotalSize();
	long GetCurrentOffset();
	long GetPacketNumber();
	void VideoClose();

	VideoPacket Packet;
private:
	enum StreamType
	{
		ProgramAssociationType = 0,
		ConditionalAccessType = 1,
		TsDescriptionType = 2,

	};
	VideoStatus status;
	FILE * fp;
	string path;
	vector<int> pids;
	vector<pair<int,int>> es_pids;
	long size;
	long offset;
	long pack_num;
	void SetPacket(unsigned char * data);
	int SetPacketHeader(unsigned char * data);
	int SetAdaptation(unsigned char * data);
	int SetPES(unsigned char * data);
	int SetPAT(unsigned char * data);
	int SetPMT(unsigned char * data);
};

VideoHandle::VideoHandle(string file_path)
{
	path = file_path;
	status = VidOk;
}

VideoStatus VideoHandle::VideoOpen()
{
	status = VidError;

	fp = fopen(path.c_str(), "rb");
	if(fp != NULL)
	{
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		offset = 0;
		pack_num = 0;
		status = VidOk;
	}

#if DEBUG_LOG
	cout << "File: " << path << "\n" << "Size: " << size << "\n"; 
#endif
	return status;
}

VideoStatus VideoHandle::NextPacket()
{
	unsigned char sync = 0;
	unsigned char pack[188];
	VideoPacket * p = &Packet;
	status = VidError;

	while (sync != EOF && sync != 0x47)
	{
		fread(&sync, 1, 1, fp);
		offset++;
	}

	if(sync == 0x47)
	{
		pack[0] = sync;
		if(fread(&pack[1], 1, 187, fp) == 187)
		{
			SetPacket(pack);
			offset += 188;
			status = VidOk;
		}
	}

	return status;
}

vector<int> VideoHandle::GetPIDs()
{
	return pids;
}

long VideoHandle::GetCurrentOffset()
{
	return offset;
}

long VideoHandle::GetTotalSize()
{
	return size;
}

long VideoHandle::GetPacketNumber()
{
	return pack_num;
}

void VideoHandle::SetPacket(unsigned char * data)
{
	unsigned char idx = 0;
	bool existing_pid = false, program_stream = false;

	idx += SetPacketHeader(data);

	if (Packet.adaptation_field == 2 || Packet.adaptation_field == 3)
		idx += SetAdaptation(data + idx);

	if (Packet.unit_start_ind)
	{
		int pointer_len = data[idx++];
#if DEBUG_LOG
		cout << "\n\tPointer field length: " << dec << pointer_len;
#endif
		idx += pointer_len;
	}

	switch(Packet.program_id)
	{
	case ProgramAssociationType:	//program association table
		idx += SetPAT(data+idx);
		break;
	case ConditionalAccessType:		//conditional access table
		break;
	case TsDescriptionType:			//tranport stream description table
		break;
	case 0x1FFF:
#if DEBUG_LOG
		cout << "\nNULL packet";
#endif
		break;
	default:

		//check if PID is existing program stream
		for (vector<int>::iterator it = pids.begin(); it < pids.end(); it++)
		{
			if (*it == Packet.program_id)
			{
				existing_pid = true;
				break;
			}
		}

		//check if PID is existing elementary stream
		if (!existing_pid)
		{
			for (vector<pair<int, int>>::iterator it = es_pids.begin(); it < es_pids.end(); it++)
			{
				if (it->first == Packet.program_id)
				{
					existing_pid = true;
					break;
				}
			}
		}

		if (existing_pid)
		{
			if (data[idx - 1] == 0 && data[idx] == 0 && data[idx + 1] == 1)
			{
				idx--;
				idx += SetPES(data + idx);
			}
			//else
			//	idx += SetPMT(data + idx);
		}
		else
			pids.push_back(Packet.program_id);

		break;
	}
}

int VideoHandle::SetPacketHeader(unsigned char * data)
{
	Packet.sync_byte =				data[0];
	Packet.transport_error_ind =	(data[1] & 0x80) >> 7;
	Packet.unit_start_ind =			(data[1] & 0x40) >> 6;
	Packet.priority =				(data[1] & 0x20) >> 5;
	Packet.program_id =				((data[1] & 0x10) << 8) | ((data[1] & 0x0F) << 8) | data[2];
	Packet.transport_scramble =		(data[3] & 0xC0) >> 6;
	Packet.adaptation_field =		(data[3] & 0x30) >> 4;
	Packet.continuity_ctr =			data[3] & 0x0F;
	pack_num++;

#if DEBUG_LOG
	cout << dec << "Packet #" << pack_num <<
			hex << "\n\tsync byte: " << (int)Packet.sync_byte << 
					"\n\ttransport error indicator: " << (int)Packet.transport_error_ind << 
					"\n\tunit start indicator: " << (int)Packet.unit_start_ind << 
					"\n\ttransport priority: " << (int)Packet.priority <<
			dec << "\n\tprogram id: " << Packet.program_id <<
			hex << "\n\ttransport scramble: " << (int)Packet.transport_scramble << 
					"\n\tadaptation field: " << (int)Packet.adaptation_field << 
					"\n\tcontinuity counter: " << (int)Packet.continuity_ctr;
#endif
	return 4;
}

int VideoHandle::SetAdaptation(unsigned char * data)
{
	unsigned char idx = 0;
	
	Packet.adaptation.length = data[idx++];

	Packet.adaptation.discontinuity_indicator =					(data[idx] & 0x80) >> 7;
	Packet.adaptation.random_access_indicator =					(data[idx] & 0x40) >> 6;
	Packet.adaptation.elementary_stream_priority_indicator =	(data[idx] & 0x20) >> 5;
	Packet.adaptation.PCR_flag =								(data[idx] & 0x10) >> 4;
	Packet.adaptation.OPCR_flag =								(data[idx] & 0x08) >> 3;
	Packet.adaptation.splicing_point_flag =						(data[idx] & 0x04) >> 2;
	Packet.adaptation.transport_private_data_flag =				(data[idx] & 0x02) >> 1;
	Packet.adaptation.adaptation_field_extension_flag =			data[idx] & 0x01;
	idx++;
	
	if(Packet.adaptation.length > 0)
	{
		//PCR
		if(Packet.adaptation.PCR_flag)
		{
			Packet.adaptation.program_clock_reference_base =		data[idx] << 25 | data[idx+1] << 17 | data[idx+2] << 9 | data[idx+3] << 1 | data[idx+4] & 0x80 >> 7;
			Packet.adaptation.reserved =							(data[idx+4] & 0x70) >> 1 | (data[idx+4] & 0x0E) >> 1;
			Packet.adaptation.program_clock_reference_extension =	(data[idx+4] & 0x01) << 8 | data[idx+5];
			idx += 6;
		}
		else
		{
			Packet.adaptation.program_clock_reference_base =		0;
			Packet.adaptation.reserved =							0;
			Packet.adaptation.program_clock_reference_extension =	0;
		}

		//OPCR
		if(Packet.adaptation.OPCR_flag)
		{
			Packet.adaptation.original_program_clock_reference_base =		data[2] << 25 | data[3] << 17 | data[4] << 9 | data[5] << 1 | data[6] & 0x80 >> 7;
			Packet.adaptation.reserved2 =									(data[idx+4] & 0x70) >> 1 | (data[idx+4] & 0x0E) >> 1;
			Packet.adaptation.original_program_clock_reference_extension =	(data[idx+4] & 0x01) << 8 | data[idx+5];
			idx += 6;
		}
		else
		{
			Packet.adaptation.original_program_clock_reference_base =		0;
			Packet.adaptation.reserved2 =									0;
			Packet.adaptation.original_program_clock_reference_extension =	0;
		}

		//Splice
		if(Packet.adaptation.splicing_point_flag)
			Packet.adaptation.splice_countdown = data[idx++];
		else
			Packet.adaptation.splice_countdown = 0;

		//Private
		if(Packet.adaptation.transport_private_data_flag)
		{
			Packet.adaptation.transport_private_data_length = data[idx++];
			Packet.adaptation.private_data_byte = new unsigned char[Packet.adaptation.transport_private_data_length];
			for(int i = 0; i < Packet.adaptation.transport_private_data_length; i++)
				Packet.adaptation.private_data_byte[i] = data[idx++];
		}
		else
		{
			Packet.adaptation.transport_private_data_length = 0;
			Packet.adaptation.private_data_byte = NULL;
		}

		//Extension
		if(Packet.adaptation.adaptation_field_extension_flag)
		{
			Packet.adaptation.adaptation_field_extension_length =	data[idx++];
			Packet.adaptation.ltw_flag =							(data[idx] & 0x80) >> 7;
			Packet.adaptation.piecewise_rate_flag =					(data[idx] & 0x40) >> 6;
			Packet.adaptation.seamless_splice_flag =				(data[idx] & 0x20) >> 5;
			Packet.adaptation.reserved3 =							data[idx++] & 0x1F;

			if(Packet.adaptation.ltw_flag)
			{
				Packet.adaptation.ltw_valid_flag =	(data[idx] & 0x80) >> 7;
				Packet.adaptation.ltw_offset =		(data[idx] & 0x7F) << 8 | data[idx];
				idx += 2;
			}
			else
			{
				Packet.adaptation.ltw_valid_flag =	0;
				Packet.adaptation.ltw_offset =		0;
			}

			if(Packet.adaptation.piecewise_rate_flag)
			{
				Packet.adaptation.reserved4 =		(data[idx] & 0xB0) >> 6;
				Packet.adaptation.piecewise_rate =	(data[idx] & 0x3F) << 16 | data[idx+1] << 8 | data[idx+2];
				idx += 3;
			}
			else
			{
				Packet.adaptation.reserved4 =		0;
				Packet.adaptation.piecewise_rate =	0;
			}

			if(Packet.adaptation.seamless_splice_flag)
			{
				Packet.adaptation.splice_type =		(data[idx] & 0xF0) >> 4;
				Packet.adaptation.DTS_next_AU =		(data[idx] & 0x0E) >> 1;
				Packet.adaptation.marker_bit =		data[idx++] & 0x01;
				Packet.adaptation.DTS_next_AU2 =	(data[idx] & 0xFE) >> 1;
				Packet.adaptation.marker_bit2 =		data[idx++] & 0x01;
				Packet.adaptation.DTS_next_AU3 =	(data[idx] & 0xFE) >> 1;
				Packet.adaptation.marker_bit3 =		data[idx++] & 0x01;
			}
			else
			{
				Packet.adaptation.splice_type =  0;
				Packet.adaptation.DTS_next_AU =	 0;
				Packet.adaptation.marker_bit =	 0;
				Packet.adaptation.DTS_next_AU2 = 0;
				Packet.adaptation.marker_bit2 =  0;
				Packet.adaptation.DTS_next_AU3 = 0;
				Packet.adaptation.marker_bit3 =  0;
			}
		}
		else
		{
			Packet.adaptation.adaptation_field_extension_length =	0;
			Packet.adaptation.ltw_flag =							0;
			Packet.adaptation.piecewise_rate_flag =					0;
			Packet.adaptation.seamless_splice_flag =				0;
			Packet.adaptation.reserved3 =							0;
			Packet.adaptation.ltw_valid_flag =						0;
			Packet.adaptation.ltw_offset =							0;
			Packet.adaptation.reserved4 =							0;
			Packet.adaptation.piecewise_rate =						0;
			Packet.adaptation.splice_type =							0;
			Packet.adaptation.DTS_next_AU =							0;
			Packet.adaptation.marker_bit =							0;
			Packet.adaptation.DTS_next_AU2 =						0;
			Packet.adaptation.marker_bit2 =							0;
			Packet.adaptation.DTS_next_AU3 =						0;
			Packet.adaptation.marker_bit3 =							0;
		}
	}

#if DEBUG_LOG
	cout << "\n\tAdaptation Field\n\t\tLength: " << dec << (int)Packet.adaptation.length <<
			"\n\t\tdiscontinuity_indicator: " << hex << (int)Packet.adaptation.discontinuity_indicator <<
			"\n\t\trandom_access_indicator: " << (int)Packet.adaptation.random_access_indicator <<
			"\n\t\telementary_stream_priority_indicator: " << (int)Packet.adaptation.elementary_stream_priority_indicator <<
			"\n\t\tPCR_flag: " << (int)Packet.adaptation.PCR_flag <<
			"\n\t\tOPCR_flag: " << (int)Packet.adaptation.OPCR_flag <<
			"\n\t\tsplicing_point_flag: " << (int)Packet.adaptation.splicing_point_flag <<
			"\n\t\ttransport_private_data_flag: " << (int)Packet.adaptation.transport_private_data_flag <<
			"\n\t\tadaptation_field_extension_flag: " << (int)Packet.adaptation.adaptation_field_extension_flag <<
			dec << "\n\t\tprogram_clock_reference_base: " << (int)Packet.adaptation.program_clock_reference_base <<
			hex << "\n\t\treserved: " << (int)Packet.adaptation.reserved <<
			dec << "\n\t\tprogram_clock_reference_extension: " << (int)Packet.adaptation.program_clock_reference_extension <<
			"\n\t\toriginal_program_clock_reference_base: " << (int)Packet.adaptation.original_program_clock_reference_base <<
			hex << "\n\t\treserved: " << (int)Packet.adaptation.reserved2 <<
			dec << "\n\t\toriginal_program_clock_reference_extension: " << (int)Packet.adaptation.original_program_clock_reference_extension <<
			"\n\t\tsplice_countdown: " << (int)Packet.adaptation.splice_countdown <<
			"\n\t\ttransport_private_data_length: " << (int)Packet.adaptation.transport_private_data_length <<
			//"\n\t\tprivate_data_byte: " << Packet.adaptation.private_data_byte <<
			"\n\t\tadaptation_field_extension_length: " << (int)Packet.adaptation.adaptation_field_extension_length <<
			hex << "\n\t\tltw_flag: " << (int)Packet.adaptation.ltw_flag <<
			"\n\t\tpiecewise_rate_flag: " << (int)Packet.adaptation.piecewise_rate_flag <<
			"\n\t\tseamless_splice_flag: " << (int)Packet.adaptation.seamless_splice_flag <<
			"\n\t\treserved: " << (int)Packet.adaptation.reserved3 <<
			"\n\t\tltw_valid_flag: " << (int)Packet.adaptation.ltw_valid_flag <<
			"\n\t\tltw_offset: " << (int)Packet.adaptation.ltw_offset <<
			"\n\t\treserved: " << (int)Packet.adaptation.reserved4 <<
			"\n\t\tpiecewise_rate: " << (int)Packet.adaptation.piecewise_rate <<
			"\n\t\tsplice_type: " << (int)Packet.adaptation.splice_type <<
			"\n\t\tDTS_next_AU: " << (int)Packet.adaptation.DTS_next_AU <<
			"\n\t\tmarker_bit: " << (int)Packet.adaptation.marker_bit <<
			"\n\t\tDTS_next_AU2: " << (int)Packet.adaptation.DTS_next_AU2 <<
			"\n\t\tmarker_bit2: " << (int)Packet.adaptation.marker_bit2 <<
			"\n\t\tDTS_next_AU3: " << (int)Packet.adaptation.DTS_next_AU3 <<
			"\n\t\tmarker_bit3: " << (int)Packet.adaptation.marker_bit3;
#endif
	return Packet.adaptation.length + 1;
}

int VideoHandle::SetPES(unsigned char * data)
{
	int idx = 0;

	Packet.pes.one_zero = 0;
	Packet.pes.PES_scrambling_control = 0;
	Packet.pes.PES_priority = 0;
	Packet.pes.data_alignment_indicator = 0;
	Packet.pes.copyright = 0;
	Packet.pes.original_or_copy = 0;

	Packet.pes.PTS_DTS_flags = 0;
	Packet.pes.ESCR_flag = 0;
	Packet.pes.ES_rate_flag = 0;
	Packet.pes.DSM_trick_mode_flag = 0;
	Packet.pes.additional_copy_info_flag = 0;
	Packet.pes.PES_CRC_flag = 0;
	Packet.pes.PES_extension_flag = 0;
	Packet.pes.PES_header_data_length = 0;

	Packet.pes.packet_start_code_prefix = (data[idx] << 16) | (data[idx+1] << 8) | data[idx+2];
	Packet.pes.stream_id = data[idx+3];
	Packet.pes.PES_packet_length = (data[idx+4] << 8) | data[idx+5];
	idx += 6;

	if( (Packet.pes.stream_id != 0xBC) &&	//program_stream_map
		(Packet.pes.stream_id != 0xBE) &&	//padding_stream
		(Packet.pes.stream_id != 0xBF) &&	//private_stream_2
		(Packet.pes.stream_id != 0xF0) &&	//ECM
		(Packet.pes.stream_id != 0xF1) &&	//EMM
		(Packet.pes.stream_id != 0xFF) &&	//program_stream_directory
		(Packet.pes.stream_id != 0xF2) &&	//DSMCC_stream
		(Packet.pes.stream_id != 0xF8) )	//ITU-T Rec. H.222.1 type E stream
	{
		Packet.pes.one_zero =					(data[idx] & 0xC0) >> 6;
		Packet.pes.PES_scrambling_control =		(data[idx] & 0x30) >> 4;
		Packet.pes.PES_priority =				(data[idx] & 0x08) >> 3;
		Packet.pes.data_alignment_indicator =	(data[idx] & 0x04) >> 2;
		Packet.pes.copyright =					(data[idx] & 0x02) >> 1;
		Packet.pes.original_or_copy =			data[idx] & 0x01;
		idx++;
		
		Packet.pes.PTS_DTS_flags =				(data[idx] & 0xC0) >> 6;
		Packet.pes.ESCR_flag =					(data[idx] & 0x20) >> 5;
		Packet.pes.ES_rate_flag =				(data[idx] & 0x10) >> 4;
		Packet.pes.DSM_trick_mode_flag =		(data[idx] & 0x08) >> 3;
		Packet.pes.additional_copy_info_flag =	(data[idx] & 0x04) >> 2;
		Packet.pes.PES_CRC_flag =				(data[idx] & 0x02) >> 1;
		Packet.pes.PES_extension_flag =			data[idx] & 0x01;
		idx++;

		Packet.pes.PES_header_data_length =		data[idx];
		idx++;

		if(Packet.pes.PTS_DTS_flags == 2 || Packet.pes.PTS_DTS_flags == 3)
		{
			Packet.pes.pts.sync =			(data[idx] & 0xF0) >> 4;
			Packet.pes.pts.pts =			((data[idx] & 0x0E) >> 1) | (data[idx + 1] << 8) | ((data[idx + 2] & 0xFE) >> 1) | (data[idx + 3] << 8) | ((data[idx + 4] & 0xFE) >> 1);
			Packet.pes.pts.marker_bit_1 =	data[idx] & 0x01;
			Packet.pes.pts.marker_bit_2 =	data[idx + 2] & 0x01;
			Packet.pes.pts.marker_bit_3 =	data[4] & 0x01;
			idx += 5;
		}
		if(Packet.pes.PTS_DTS_flags == 3)
		{
			Packet.pes.dts.sync =			(data[0] & 0xF0) >> 4;
			Packet.pes.dts.dts =			((data[0] & 0x0E) >> 1) | (data[1] << 8) | ((data[2] & 0xFE) >> 1) | (data[3] << 8) | ((data[4] & 0xFE) >> 1);
			Packet.pes.dts.marker_bit_1 =	data[0] & 0x01;
			Packet.pes.dts.marker_bit_2 =	data[2] & 0x01;
			Packet.pes.dts.marker_bit_3 =	data[4] & 0x01;
			idx += 5;
		}
		if(Packet.pes.ESCR_flag == 1)
		{
			Packet.pes.escr.reserved =			(data[0] & 0xC0) >> 6;
			Packet.pes.escr.escr_base =			((data[0] & 0x38) >> 3) | ((data[0] & 0x03) << 13) | (data[1] << 6) | 
												((data[2] & 0xF8) >> 3) | ((data[2] & 0x03) << 13) | (data[3] << 6) | 
												((data[4] & 0xF8) >> 3);
			Packet.pes.escr.marker_bit_1 =		(data[0] & 0x04) >> 2;
			Packet.pes.escr.marker_bit_2 =		(data[2] & 0x04) >> 2;
			Packet.pes.escr.marker_bit_3 =		(data[4] & 0x04) >> 2;
			Packet.pes.escr.escr_extension =	((data[4] & 0x03) << 7) | ((data[5] & 0xFE) >> 1);
			Packet.pes.escr.marker_bit_4 =		data[5] & 0x01;
			idx += 6;
		}
		if(Packet.pes.ES_rate_flag == 1)
		{
			Packet.pes.es.marker_bit_1 =	(data[0] & 0x80) >> 7;
			Packet.pes.es.ES_rate =			((data[0] & 0x7F) << 14) | (data[1] << 6) | ((data[2] & 0xFE) >> 1);
			Packet.pes.es.marker_bit_2 =	data[2] & 0x01;
			idx += 3;
		}
		if(Packet.pes.DSM_trick_mode_flag == 1)
		{
			Packet.pes.dsm.trick_mode_control = (data[idx] & 0xE0) >> 5;

			switch(Packet.pes.dsm.trick_mode_control)
			{
			case 0:			//fast forward
				Packet.pes.dsm.field_id =				(data[idx] & 0x18) >> 3;
				Packet.pes.dsm.intra_slice_refresh =	(data[idx] & 0x04) >> 2;
				Packet.pes.dsm.frequency_truncation =	data[idx] & 0x03;
				Packet.pes.dsm.rep_cntrl =				0;
				Packet.pes.dsm.reserved =				0;
				break;
			case 1:			//slow motion
				Packet.pes.dsm.rep_cntrl = data[idx] & 0x1F;
				break;
			case 2:			//freeze frame
				Packet.pes.dsm.field_id = (data[idx] & 0x18) >> 3;
				Packet.pes.dsm.reserved = data[idx] & 0x07;
				break;
			case 3:			//fast reverse
				Packet.pes.dsm.field_id =				(data[idx] & 0x18) >> 3;
				Packet.pes.dsm.intra_slice_refresh =	(data[idx] & 0x04) >> 2;
				Packet.pes.dsm.frequency_truncation =	data[idx] & 0x03;
				break;
			case 4:			//slow reverse
				Packet.pes.dsm.rep_cntrl = data[idx] & 0x1F;
				break;
			default:		//reserved
				Packet.pes.dsm.reserved = data[idx] & 0x1F;
				break;
			}
			idx++;
		}
		if(Packet.pes.additional_copy_info_flag == 1)
		{
			Packet.pes.additional.marker_bit = (data[idx] & 0x80) >> 7;
			Packet.pes.additional.copy_info = data[idx] & 0x7F;
			idx++;
		}
		if(Packet.pes.PES_CRC_flag == 1)
		{
			Packet.pes.crc.previous_CRC = (data[idx] << 8) | data[idx+1];
			idx += 2;
		}
		if (Packet.pes.PES_extension_flag == 1)
		{
			Packet.pes.ext.private_data_flag = (data[idx] & 0x80) >> 7;
			Packet.pes.ext.pack_header_field_flag = (data[idx] & 0x40) >> 6;
			Packet.pes.ext.program_packet_sequence_counter_flag = (data[idx] & 0x20) >> 5;
			Packet.pes.ext.P_STD_buffer_flag = (data[idx] & 0x10) >> 4;
			Packet.pes.ext.reserved = (data[idx] & 0x0E) >> 1;
			Packet.pes.ext.extension_flag_2 = data[idx] & 0x01;
			idx++;

			if (Packet.pes.ext.private_data_flag == 1)
			{
				Packet.pes.ext.private_data = new unsigned char[8];
				for (int i = 0; i < 8; i++)
					Packet.pes.ext.private_data[i] = data[idx + i];
				idx += 8;
			}

			if (Packet.pes.ext.pack_header_field_flag == 1)
			{
				Packet.pes.ext.pack_Field_length = data[idx];
				idx++;
			}

			if (Packet.pes.ext.program_packet_sequence_counter_flag == 1)
			{
				Packet.pes.ext.marker_bit1 = (data[idx] & 0x80) >> 7;
				Packet.pes.ext.program_packet_sequence_counter = data[idx] & 0x7F;
				Packet.pes.ext.marker_bit2 = (data[idx + 1] & 0x80) >> 7;
				Packet.pes.ext.MPEG1_MPEG2_identifier = (data[idx + 1] & 0x40) >> 6;
				Packet.pes.ext.original_stuff_length = data[idx + 1] & 0x3F;
				idx += 2;
			}

			if (Packet.pes.ext.P_STD_buffer_flag == 1)
			{
				Packet.pes.ext.zero_one = (data[idx] & 0xC0) >> 6;
				Packet.pes.ext.P_STD_buffer_scale = (data[idx] & 0x20) >> 5;
				Packet.pes.ext.P_STD_buffer_size = (data[idx] & 0x1F) << 8 | data[idx + 1];
				idx += 2;
			}

			if (Packet.pes.ext.extension_flag_2 == 1)
			{
				Packet.pes.ext.marker_bit3 = (data[idx] & 0x80) >> 7;
				Packet.pes.ext.PES_extension_field_length = data[idx] & 0x7F;
			}
		}
	}
	else if ((Packet.pes.stream_id == 0xBC) &&	//program_stream_map
			(Packet.pes.stream_id == 0xBF) &&	//private_stream_2
			(Packet.pes.stream_id == 0xF0) &&	//ECM
			(Packet.pes.stream_id == 0xF1) &&	//EMM
			(Packet.pes.stream_id == 0xFF) &&	//program_stream_directory
			(Packet.pes.stream_id == 0xF2) &&	//DSMCC_stream
			(Packet.pes.stream_id == 0xF8))		//ITU-T Rec. H.222.1 type E stream)
	{

	}
#if DEBUG_LOG
	cout << hex << "\n\tPES\n\t\tPacket start code prefix: " << Packet.pes.packet_start_code_prefix <<
		dec << "\n\t\tStream ID: " << (int)Packet.pes.stream_id <<
		"\n\t\tPES_packet_length: " << (int)Packet.pes.PES_packet_length <<
		hex << "\n\t\t01: " << (int)Packet.pes.one_zero <<
		"\n\t\tPES_scrambling_control: " << (int)Packet.pes.PES_scrambling_control <<
		"\n\t\tPES priority: " << (int)Packet.pes.PES_priority <<
		"\n\t\tData alignment indicator: " << (int)Packet.pes.data_alignment_indicator <<
		"\n\t\tCopyright: " << (int)Packet.pes.copyright <<
		"\n\t\tOriginal or copy: " << (int)Packet.pes.original_or_copy <<
		"\n\t\tPTS DTS flags: " << (int)Packet.pes.PTS_DTS_flags <<
		"\n\t\tESCR flag: " << (int)Packet.pes.ESCR_flag <<
		"\n\t\tES rate flag: " << (int)Packet.pes.ES_rate_flag <<
		"\n\t\tDSM trick mode flag: " << (int)Packet.pes.DSM_trick_mode_flag <<
		"\n\t\tAdditional copy info flag: " << (int)Packet.pes.additional_copy_info_flag <<
		"\n\t\tPES CRC flag: " << (int)Packet.pes.PES_CRC_flag <<
		"\n\t\tPES extension flag: " << (int)Packet.pes.PES_extension_flag <<
		dec << "\n\t\tPES header data length: " << (int)Packet.pes.PES_header_data_length;

	if (Packet.pes.PTS_DTS_flags == 2 || Packet.pes.PTS_DTS_flags == 3)
	{
		cout << hex << "\n\t\t\tPTS sync: " << (int)Packet.pes.pts.sync <<
			dec << "\n\t\t\tPTS: " << (int)Packet.pes.pts.pts <<
			"\n\t\t\tPTS marker 1: " << (int)Packet.pes.pts.marker_bit_1 <<
			"\n\t\t\tPTS marker 2: " << (int)Packet.pes.pts.marker_bit_2 <<
			"\n\t\t\tPTS marker 3: " << (int)Packet.pes.pts.marker_bit_3;
	}
	if (Packet.pes.PTS_DTS_flags == 3)
	{
		cout << hex << "\n\t\t\tDTS sync: " << (int)Packet.pes.dts.sync <<
			dec << "\n\t\t\tDTS: " << (int)Packet.pes.dts.dts <<
			"\n\t\t\tDTS marker 1: " << (int)Packet.pes.dts.marker_bit_1 <<
			"\n\t\t\tDTS marker 2: " << (int)Packet.pes.dts.marker_bit_2 <<
			"\n\t\t\tDTS marker 3: " << (int)Packet.pes.dts.marker_bit_3;
	}
	if (Packet.pes.ESCR_flag == 1)
	{
		cout << hex << "\n\t\t\tESCR reseved: " << (int)Packet.pes.escr.reserved << 
			"\n\t\t\tESCR base: " << (int)Packet.pes.escr.escr_base << 
			"\n\t\t\tESCR marker 1: " << (int)Packet.pes.escr.marker_bit_1 <<
			"\n\t\t\tESCR marker 2: " << (int)Packet.pes.escr.marker_bit_2 <<
			"\n\t\t\tESCR marker 3: " << (int)Packet.pes.escr.marker_bit_3 <<
			"\n\t\t\tESCR extension: " << (int)Packet.pes.escr.escr_extension <<
			"\n\t\t\tESCR marker 4: " << (int)Packet.pes.escr.marker_bit_4;
	}
	if (Packet.pes.ES_rate_flag == 1)
	{
		cout << hex << "\n\t\t\tES marker 1: " << (int)Packet.pes.es.marker_bit_1 <<
			"\n\t\t\tES rate: " << (int)Packet.pes.es.ES_rate <<
			"\n\t\t\tES marker 2: " << (int)Packet.pes.es.marker_bit_2;
	}
	if (Packet.pes.DSM_trick_mode_flag == 1)
	{
		cout << hex << "\n\t\tTrick mode control: " << (int)Packet.pes.dsm.trick_mode_control;

		switch (Packet.pes.dsm.trick_mode_control)
		{
		case 0:			//fast forward
			cout << dec << "\n\t\t\tField ID: " << (int)Packet.pes.dsm.field_id <<
				"\n\t\t\tIntra slice refresh: " << (int)Packet.pes.dsm.intra_slice_refresh <<
				"\n\t\t\tFrequency truncation: " << (int)Packet.pes.dsm.frequency_truncation <<
				"\n\t\t\tRepition control: " << (int)Packet.pes.dsm.rep_cntrl <<
				"\n\t\t\tReserved: " << (int)Packet.pes.dsm.reserved;
			break;
		case 1:			//slow motion
			cout << dec << "\n\t\t\tRepition control: " << (int)Packet.pes.dsm.rep_cntrl;
			break;
		case 2:			//freeze frame
			cout << dec << "\n\t\t\tField ID: " << (int)Packet.pes.dsm.field_id <<
				"\n\t\t\tReserved: " << (int)Packet.pes.dsm.reserved;
			break;
		case 3:			//fast reverse
			cout << dec << "\n\t\t\tField ID: " << (int)Packet.pes.dsm.field_id <<
				"\n\t\t\tIntra slice refresh: " << (int)Packet.pes.dsm.intra_slice_refresh <<
				"\n\t\t\tFrequency truncation: " << (int)Packet.pes.dsm.frequency_truncation;
			break;
		case 4:			//slow reverse
			cout << dec << "\n\t\t\tRepition control: " << (int)Packet.pes.dsm.rep_cntrl;
			break;
		default:		//reserved
			cout << dec << "\n\t\t\tReserved: " << (int)Packet.pes.dsm.reserved;
			break;
		}
	}
	if (Packet.pes.additional_copy_info_flag == 1)
	{
		cout << hex << "\n\t\tAdditional copy info marker: " << (int)Packet.pes.additional.marker_bit <<
			"\n\t\tAdditional copy info: " << (int)Packet.pes.additional.copy_info;
	}
	if (Packet.pes.PES_CRC_flag == 1)
	{
		cout << hex << "\n\t\tCRC: " << (int)Packet.pes.crc.previous_CRC;
	}
	if (Packet.pes.PES_extension_flag == 1)
	{
		cout << hex << "\n\t\tPrivate data flag: " << (int)Packet.pes.ext.private_data_flag <<
			"\n\t\tPack header field flag: " << (int)Packet.pes.ext.pack_header_field_flag <<
			"\n\t\tProgram packet sequence counter flag: " << (int)Packet.pes.ext.program_packet_sequence_counter_flag <<
			"\n\t\tP-STD buffer flag: " << (int)Packet.pes.ext.P_STD_buffer_flag <<
			"\n\t\tReserved: " << (int)Packet.pes.ext.reserved <<
			"\n\t\tExtension flag 2: " << (int)Packet.pes.ext.extension_flag_2;
		if (Packet.pes.ext.private_data_flag == 1)
		{
			cout << "\n\t\tPrivate data:";
			for (int i = 0; i < 8; i++)
				cout << hex << " " << (int)Packet.pes.ext.private_data[i];
		}
		if (Packet.pes.ext.pack_header_field_flag == 1)
		{
			cout << dec << "\n\t\tPack field length: " << (int)Packet.pes.ext.pack_Field_length;
		}
		if (Packet.pes.ext.program_packet_sequence_counter_flag == 1)
		{
			cout << hex << "\n\t\tMarker bit: " << (int)Packet.pes.ext.marker_bit1 <<
				dec << "\n\t\tProgram packet sequence counter: " << (int)Packet.pes.ext.program_packet_sequence_counter <<
				"\n\t\tMarker bit: " << (int)Packet.pes.ext.marker_bit2 <<
				"\n\t\tMPEG1_MPEG2 ID: " << (int)Packet.pes.ext.MPEG1_MPEG2_identifier <<
				"\n\t\tOriginal stuff length: " << (int)Packet.pes.ext.original_stuff_length;
		}
		if (Packet.pes.ext.P_STD_buffer_flag == 1)
		{
			cout << hex << "\n\t\t01: " << (int)Packet.pes.ext.zero_one <<
				dec << "\n\t\tP-STD buffer scale: " << (int)Packet.pes.ext.P_STD_buffer_scale <<
				"\n\t\tP-STD buffer size: " << (int)Packet.pes.ext.P_STD_buffer_size;
		}
		if (Packet.pes.ext.extension_flag_2 == 1)
		{
			cout << hex << "\n\t\tMarker bit: " << (int)Packet.pes.ext.marker_bit3 <<
				"\n\t\tExtension field length: " << (int)Packet.pes.ext.PES_extension_field_length;
		}
	}
	if (Packet.pes.stream_id == 0xBC) { cout << "\n\t\tPES program section"; }
	else if (Packet.pes.stream_id == 0xBD) { cout << "\n\t\tPES private stream 1"; }
	else if (Packet.pes.stream_id == 0xBE) { cout << "\n\t\tPES padding stream"; }
	else if (Packet.pes.stream_id == 0xBF) { cout << "\n\t\tPES private stream 2"; }
	else if (Packet.pes.stream_id >= 0xC0 && Packet.pes.stream_id <= 0xDF) { cout << "\n\t\tPES audio stream"; }
	else if (Packet.pes.stream_id >= 0xE0 && Packet.pes.stream_id <= 0xEF) { cout << "\n\t\tPES video stream"; }
	else if (Packet.pes.stream_id == 0xF0) { cout << "\n\t\tPES ECM stream"; }
	else if (Packet.pes.stream_id == 0xF1) { cout << "\n\t\tPES EMM stream"; }
	else if (Packet.pes.stream_id == 0xF2) { cout << "\n\t\tPES H.222 | ISO IEC 13818 -1 or -3 or 14496-3"; }
	else if (Packet.pes.stream_id == 0xF3) { cout << "\n\t\tPES ISO IEC 13522 stream directory"; }
	else if (Packet.pes.stream_id == 0xF4) { cout << "\n\t\tPES H.222 Type A"; }
	else if (Packet.pes.stream_id == 0xF5) { cout << "\n\t\tPES H.222 Type B"; }
	else if (Packet.pes.stream_id == 0xF6) { cout << "\n\t\tPES H.222 Type C"; }
	else if (Packet.pes.stream_id == 0xF7) { cout << "\n\t\tPES H.222 Type D"; }
	else if (Packet.pes.stream_id == 0xF8) { cout << "\n\t\tPES H.222 Type E"; }
	else if (Packet.pes.stream_id == 0xF9) { cout << "\n\t\tPES ancillary stream"; }
	else if (Packet.pes.stream_id == 0xFA) { cout << "\n\t\tPES ISO IEC 14496-1 SL packetized stream"; }
	else if (Packet.pes.stream_id == 0xFB) { cout << "\n\t\tPES ISO IEC 14496-1 FlexMux stream"; }
	else if (Packet.pes.stream_id == 0xFF) { cout << "\n\t\tPES program stream directory"; }
#endif

	return idx;
}

int VideoHandle::SetPAT(unsigned char * data)
{
	int idx = 0;
	bool existing_pid = false;

	Packet.pat.table_id =					data[idx];
	Packet.pat.section_syntax_indicator =	(data[idx+1] & 0x80) >> 7;
	Packet.pat.zero =						(data[idx+1] & 0x40) >> 6;
	Packet.pat.reserved =					(data[idx+1] & 0x30) >> 4;
	Packet.pat.section_length =				(data[idx+1] & 0x0F) << 8 | data[idx+2];
	Packet.pat.transport_stream_id =		data[idx+3] << 8 | data[idx+4];
	Packet.pat.reserved2 =					(data[idx+5] & 0xC0) >> 6;
	Packet.pat.version_number =				(data[idx+5] & 0x3E) >> 1;
	Packet.pat.current_next_indicator =		data[idx+5] & 0x01;
	Packet.pat.section_number =				data[idx+6];
	Packet.pat.last_section_number =		data[idx+7];
	idx += 8;

	//(re-)initialize vectors
	Packet.pat.program_number.clear();
	Packet.pat.reserved3.clear();
	Packet.pat.network_PID.clear();
	Packet.pat.program_map_PID.clear();

	int num_of_programs = (Packet.pat.section_length - 5 - 4) / 4; //subtract part of header and crc
	for (int i = 0; i < num_of_programs; i++)
	{
		Packet.pat.program_number.push_back(data[idx] << 8 | data[idx+1]);
		Packet.pat.reserved3.push_back((data[idx+2] & 0xE0) >> 5);
		int pid = (data[idx + 2] & 0x1F) << 8 | data[idx + 3];
		if (Packet.pat.program_number.back() == 0)
			Packet.pat.network_PID.push_back(pid);
		else
			Packet.pat.program_map_PID.push_back(pid);
		idx += 4;

		for (vector<int>::iterator it = pids.begin(); it < pids.end(); it++)
		{
			if (*it == pid)
				existing_pid = true;
		}
		if (!existing_pid)
			pids.push_back(pid);
	}
	Packet.pat.CRC_32 = data[idx] << 24 | data[idx+1] << 16 | data[idx+2] << 8 | data[idx+3];

#if DEBUG_LOG
	cout << dec << "\n\tProgram Associate Table\n\t\tTable ID: " << (int)Packet.pat.table_id <<
		hex << "\n\t\tSection Syntax Indicator: " << (int)Packet.pat.section_syntax_indicator <<
		"\n\t\t0: " << (int)Packet.pat.zero <<
		"\n\t\tReserved: " << (int)Packet.pat.reserved <<
		dec << "\n\t\tSection Length: " << (int)Packet.pat.section_length <<
		"\n\t\tTransport Stream ID: " << (int)Packet.pat.transport_stream_id <<
		"\n\t\tReserved: " << (int)Packet.pat.reserved2 <<
		"\n\t\tVersion Number: " << (int)Packet.pat.version_number <<
		hex << "\n\t\tCurrent Next Indicator: " << (int)Packet.pat.current_next_indicator <<
		dec << "\n\t\tSection Number: " << (int)Packet.pat.section_number <<
		"\n\t\tLast Section Number: " << (int)Packet.pat.last_section_number;

	for (int i = 0; i < num_of_programs; i++)
	{
		cout << "\n\t\tProgram #" << dec << (int)Packet.pat.program_number.at(i) <<
			hex << "\n\t\t\tReserved: " << (int)Packet.pat.reserved2;
		if (Packet.pat.program_number.at(i) == 0)
			cout << dec << "\n\t\t\tNetwork PID: " << (int)Packet.pat.network_PID.at(i);
		else
			cout << dec << "\n\t\t\tProgram Map PID: " << (int)Packet.pat.program_map_PID.at(i);
	}
#endif

	return idx;
}

int VideoHandle::SetPMT(unsigned char * data)
{
	int idx = 0;

	Packet.pmt.table_id =					data[idx];
	Packet.pmt.section_syntax_indicator =	(data[idx+1] & 0x80) >> 7;
	Packet.pmt.zero =						(data[idx+1] & 0x40) >> 6;
	Packet.pmt.reserved =					(data[idx+1] & 0x30) >> 4;
	Packet.pmt.section_length =				(data[idx+1] & 0x0F) << 8 | data[idx+2];
	Packet.pmt.program_number =				data[idx+3] << 8 | data[idx+4];
	Packet.pmt.reserved2 =					(data[idx+5] & 0xC0) >> 6;
	Packet.pmt.version_number =				(data[idx+5] & 0x3E) >> 1;
	Packet.pmt.current_next_indicator =		data[idx+5] & 0x01;
	Packet.pmt.section_number =				data[idx+6];
	Packet.pmt.last_section_number =		data[idx+7];
	Packet.pmt.reserved3 =					(data[idx+8] & 0xE0) >> 6;
	Packet.pmt.PCR_PID =					(data[idx+8] & 0x1F) << 8 | data[idx+9];
	Packet.pmt.reserved4 =					(data[idx+10] & 0xF0) >> 4;
	Packet.pmt.program_info_length =		(data[idx+10] & 0x0F) << 8 | data[idx+11];
	idx += 12;

	int stream_len = Packet.pmt.section_length + 3; //not including first three bytes
	Packet.pmt.descriptor.copy((char *)data, idx, Packet.pmt.program_info_length);
	idx += Packet.pmt.program_info_length;

	//(re-)initialize vectors
	Packet.pmt.stream_type.clear();
	Packet.pmt.reserved5.clear();
	Packet.pmt.elementary_PID.clear();
	Packet.pmt.reserved6.clear();
	Packet.pmt.ES_info_length.clear();
	Packet.pmt.descriptor2.clear();

	string temp;
	bool existing_pid = false;
	while ((stream_len - idx) > 4)
	{
		int type = data[idx];
		int pid = (data[idx + 1] & 0x1F) << 8 | data[idx + 2];

		Packet.pmt.stream_type.push_back(type);
		Packet.pmt.reserved5.push_back((data[idx+1] & 0xE0) >> 5);
		Packet.pmt.elementary_PID.push_back(pid);
		Packet.pmt.reserved6.push_back((data[idx + 3] & 0xF0) >> 4);
		Packet.pmt.ES_info_length.push_back((data[idx+3] & 0x0F) << 8 | data[idx+4]);
		temp.copy((char *)data, idx, Packet.pmt.ES_info_length.back());
		Packet.pmt.descriptor2.push_back(temp);
		idx += 5 + Packet.pmt.ES_info_length.back();

		for (vector<pair<int, int>>::iterator it = es_pids.begin(); it < es_pids.end(); it++)
		{
			if (it->first == pid)
				existing_pid = true;
		}
		if (!existing_pid)
			es_pids.push_back(make_pair(pid, type));
	}
	Packet.pmt.CRC_32 = data[idx] << 24 | data[idx+1] << 16 | data[idx+2] << 8 | data[idx+3];
	idx += 4;

#if DEBUG_LOG
	cout << hex << "\n\tProgram Map Table\n\t\tTable ID: " << (int)Packet.pmt.table_id <<
		"\n\t\tSection Syntax Indicator: " << (int)Packet.pmt.section_syntax_indicator <<
		"\n\t\t0: " << (int)Packet.pmt.zero <<
		"\n\t\tReserved: " << (int)Packet.pmt.reserved <<
		dec << "\n\t\tSection Length: " << (int)Packet.pmt.section_length <<
		"\n\t\tProgram Number: " << (int)Packet.pmt.program_number <<
		hex << "\n\t\tReserved: " << (int)Packet.pmt.reserved2 <<
		dec << "\n\t\tVersion Number: " << (int)Packet.pmt.version_number <<
		hex << "\n\t\tCurrent Next Indicator: " << (int)Packet.pmt.current_next_indicator <<
		"\n\t\tSection Number: " << (int)Packet.pmt.section_number <<
		"\n\t\tLast Section Number: " << (int)Packet.pmt.last_section_number <<
		"\n\t\tReserved: " << (int)Packet.pmt.reserved3 <<
		dec << "\n\t\tPCR PID: " << (int)Packet.pmt.PCR_PID <<
		"\n\t\tReserverd: " << (int)Packet.pmt.reserved4 <<
		"\n\t\tProgram Info Length: " << (int)Packet.pmt.program_info_length <<
		"\n\t\tProgram descriptor: " << Packet.pmt.descriptor;

	for (int i = 0; i < (int)Packet.pmt.stream_type.size(); i++)
	{
		cout << dec << "\n\t\tElementary Stream #" << (i+1) <<
			"\n\t\t\tStream Type: " << (int)Packet.pmt.stream_type.at(i) <<
			"\n\t\t\tReserved: " << (int)Packet.pmt.reserved5.at(i) <<
			"\n\t\t\tElementary PID: " << (int)Packet.pmt.elementary_PID.at(i) <<
			"\n\t\t\tReserved: " << (int)Packet.pmt.reserved6.at(i) <<
			"\n\t\t\tES Info Length: " << (int)Packet.pmt.ES_info_length.at(i) <<
			"\n\t\t\tES Info: " << Packet.pmt.descriptor2.at(i);
	}
#endif

	return idx;
}

void VideoHandle::VideoClose()
{
	fclose(fp);
}

int _tmain(int argc, _TCHAR* argv[])
{
	VideoHandle vid = VideoHandle("C:\\Users\\Travis\\Videos\\San_Diego_Clip.ts");
	if(vid.VideoOpen() == VidOk)
	{
		while(vid.NextPacket() == VidOk)
		{
			getchar();
			/*if (vid.GetPacketNumber() % 100 == 0)
			{
				system("cls");
				cout << vid.GetCurrentOffset() << "/" << vid.GetTotalSize() << "%";
			}*/
		}
		vid.VideoClose();
		vector<int> pids = vid.GetPIDs();
		for (int i = 1; i <= pids.size(); i++)
			std::cout << i << ": " << pids.at(i - 1) << "\n";
	}
	cout << "done\n";
	getchar();
	return 0;
}
